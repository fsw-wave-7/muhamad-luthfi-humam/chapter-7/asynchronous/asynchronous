// // Asynchronous process
// console.log("Hello Binarian!");

// // output kode akan ditunda selama 100 milliseconds
// setTimeout(() => { console.log("JavaScript")},1000)
// console.log("Developer");

// console.log('Aku keluar pertama')

// setTimeout(() => console.log('Aku keluar setelah 3 detik'), 3000)
// setTimeout(() => console.log('Apakah aku yang kedua?'), 0)

// console.log('Apakah aku yang ketiga?')

// let a = 0;
// const iniInterval = setInterval(() => {
//   console.log(`${++a} kali jalan`);
//   if (a === 10) clearInterval(iniInterval);
// }, 100);

// console.log('Apakah aku jalan duluan?');

// function isPasswordCorrect(password) {
//   return new Promise((resolve, reject) => {
//     console.log('Password:', password);
//     if (password !== '123456')
//         return reject('Wrong password!');

//     resolve('Password is correct!');
//   });
// }
// // Output-nya akan resolve: Password is correct!
// isPasswordCorrect('123456')
//   .then((resolve) => console.log(resolve))
//   .catch((reject) => console.error(reject));
// // Output-nya akan reject: Wrong password!
// isPasswordCorrect('123455')
//   .then((resolve) => console.log(resolve))
//   .catch((reject) => console.error(reject));


let punyaBuku = false;
// Function beliBuku
const beliBuku = () =>
  new Promise((resolve) => {
    setTimeout(() => {
      punyaBuku = true;
      resolve();
    }, 1000);
  });
// Function gambar meminta judul sebagai parameter
// Mengembalikan Promise
const gambar = (judul) => {
  if (!punyaBuku) return Promise.reject('Gak punya buku');
  return Promise.resolve({
    id: Date.now(),
    title: judul,
    createdAt: Date(),
  });
};
// Function utama akan menjalankan function yang lain
async function main() {
    try {
        // if (!punyaBuku) await beliBuku()
        const hasilGambar = await gambar("Doraemon")
        console.log(hasilGambar)
       }
       catch(err) {
        console.log(err)
       }
    }  
main();
